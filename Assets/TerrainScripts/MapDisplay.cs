﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapDisplay : MonoBehaviour
{

    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
    public GameObject heightMap;
    public MeshData meshData;
    DiamondSquare ds;


    public Texture2D texture;
    Color32[] cols;

    //2^k number
    public int size;
    public float scalar;
    public float rRng;
    public float dec;

    int mapSize;

    public bool autoUpdate;
    public int numColors;
    public float[,] dsArray;



    //use for selecting the colors
    public List<Color> MapColours = new List<Color>();

    

    // Use this for initialization
    void Start()
    {
        setup();
    }


    public void setup()
    {
        DiamondSquare ds = new DiamondSquare(size, rRng, 3);
        
        mapSize = ds.dsArray.GetLength(0);

        //this must be 3/5/9/17/33
        
        texture = new Texture2D(mapSize, mapSize);
        cols = new Color32[mapSize * mapSize];


        dsArray = ds.dsArray;


        for (int y = 0; y < ds.mapSize; y++) {
            for (int x = 0; x < ds.mapSize; x++) {

                dsArray[x,y] = Mathf.InverseLerp(ds.maxVal, ds.minVal, dsArray[x,y]);
                cols[x + y * mapSize] = getColor(dsArray[x, y]);

            }
        }


        meshData = MeshGenerator.GenerateTerrainMesh(dsArray, scalar);

        reDrawMesh(meshData, texture);

    }

    public void recalColors()
    {

        for(int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                cols[x + y * size] = getColor(dsArray[x, y]);
            }
        }
        texture.SetPixels32(cols);
        texture.Apply();
    }


/// <summary>
/// Smooths a terrain by averaging 8 points around
/// every point
/// </summary>
    public void smoothTerrain()
    {
        for (int y = 0; y < mapSize; y++)
        {
            for (int x = 0; x < mapSize; x++)
            {
                
                smoothVertex(x, y);
            }
        }
    }




    /// <summary>
    /// Draws a mesh using the heightMap component
    /// 
    /// </summary>
    /// <param name="meshData">Takes a meshData as defined in MeshGenerator</param>
    /// <param name="texture">Takes a Texture2D of colours (change this)</param>
    public void reDrawMesh(MeshData meshData, Texture2D texture)
    {
        this.meshData = meshData;
        meshFilter.sharedMesh = meshData.CreateMesh();
        heightMap.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = texture;
        
    }


    private void smoothVertex(int x, int y)
    {
        float tl, t, tr, l, r, bl, b, br;

        tl = getValW(x - 1, y - 1);
        t = getValW(x, y - 1);
        tr = getValW(x + 1, y - 1);
        l = getValW(x - 1, y);
        r = getValW(x + 1, y);
        bl = getValW(x - 1, y + 1);
        b = getValW(x, y + 1);
        br = getValW(x + 1, y + 1);

        float avg = (tl + t + tr + l + r + bl + b + br) / 8.0f;
        setVal(x, y, avg);
    }



    
    /*performs the square step. takes the middle point of the square as 
     * input (x,y) and gets the four corners.
     * */
    private Color getColor (float c)
    {

        if (c <= 0.3)
        {
            return MapColours[0];

        }
        else if (c <= .4)
        {
            return MapColours[1];
        }
        else if (c <= .6)
        {
            return MapColours[2];
        }
        else if (c <= .9)
        {
            return MapColours[3];
        }
        else
        {
            return MapColours[4];
        }
    }

    private void setVal(int x, int y, float val)
    {

        dsArray[x, y] = val; ;

    }

    private float getValW(int x, int y)
    {
        return dsArray[wrap(x), wrap(y)];
    }

    private int wrap(int a)
    {
        if (a > (mapSize - 1)) {
            return a - (mapSize - 1);
        }
        else if (a < 0) {
            return (mapSize - 1) + a;
        }

        return a;
    }

}


