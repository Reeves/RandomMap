﻿using UnityEngine;
using System.Collections;
using System;

public class MeshGenerator : MonoBehaviour {


    

    public static MeshData GenerateTerrainMesh(float[,] heightMap, float scalar)
    {


        int mapSize = heightMap.GetLength(0);

        float topLeftX = (mapSize - 1) / -2f;
        float topLeftZ = (mapSize - 1) / 2f;

        MeshData meshData = new MeshData(mapSize);

        int vertexIndex = 0;


        for (int y = 0; y < mapSize; y++)
        {
            for (int x = 0; x < mapSize; x++)
            {

                meshData.vertices[vertexIndex] = new Vector3(topLeftX + x, heightMap[x, y] * scalar, topLeftZ - y);
                meshData.uvs[vertexIndex] = new Vector2(x / (float)mapSize, y / (float)mapSize);

                if (x < mapSize - 1 && y < mapSize - 1)
                {
                    meshData.addTriangle(vertexIndex, vertexIndex + mapSize + 1, vertexIndex + mapSize);
                    meshData.addTriangle(vertexIndex + mapSize + 1, vertexIndex, vertexIndex + 1);
                }

                vertexIndex++;
            }
        }




        return meshData;


    }

}


public class MeshData
{
    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uvs;


    int triangleIndex;

    public MeshData(int size)
    {
        vertices = new Vector3[size * size];
        uvs = new Vector2[size * size];
        triangles = new int[(size - 1) * (size - 1) * 6];
    }

    public void addTriangle(int a, int b, int c)
    {
        triangles[triangleIndex] = a;
        triangles[triangleIndex + 1] = b;
        triangles[triangleIndex + 2] = c;
        triangleIndex += 3;
    }

    public Mesh CreateMesh()
    {

        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals();
        return mesh;
    }








}
