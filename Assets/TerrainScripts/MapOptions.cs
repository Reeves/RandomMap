﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MapDisplay))]
public class MapOptions : Editor
{

    public override void OnInspectorGUI()
    {
        MapDisplay display = (MapDisplay)target;

        if (DrawDefaultInspector())
        {
            if (display.autoUpdate)
            {
                display.reDrawMesh(MeshGenerator.GenerateTerrainMesh(display.dsArray, display.scalar), display.texture);
            }
            
        }

        if (GUILayout.Button("Generate Map"))
        {
            display.reDrawMesh(MeshGenerator.GenerateTerrainMesh(display.dsArray, display.scalar), display.texture);
        }

        if (GUILayout.Button("Smooth"))
        {
            display.smoothTerrain();
            display.reDrawMesh(MeshGenerator.GenerateTerrainMesh(display.dsArray, display.scalar), display.texture);
        }

        GUILayout.Box(new GUIContent("Choose Colors"));

        if(GUILayout.Button("Generate Colors"))
        {
            display.recalColors();
            display.reDrawMesh(MeshGenerator.GenerateTerrainMesh(display.dsArray, display.scalar), display.texture);
        }


    }
}
